# Introdução

Nome do aluno: Carlos Eduardo da Silva Cerqueira
Nome do professor: Rita Suzana Pitangueira Maciel
Semestre: 2023.1

# Quais são as classes necessárias ?

Para esse sistema, é perceptível que iremos precisar de uma classe que represente o proprietário `Proprietario` e o imovel `Imovel`. Além disso, iremos precisar de classes que representem os aplicativos em que seria possível alugar o imovel. 
Para esses aplicativos, é possível existir um classe que represente todos `AlugaApp` ou podemos criar uma classe abstrata(ou interface) `AlugaApp` que cada classe de aplicativo vai herdar(implementar).
A depender da necessidade da aplicação, talvez sejam necessárias classes representando o inquilino `Inquilino` e o contrato de locação `Contrato`.

# Como ter acesso as versões da atividade

A cada entrega, irei gerar uma tag, que pode ser utilizada para acessar uma versão específica da atividade. Cada tag irá aparecer como uma `branch` nesse repositório.
