import java.util.ArrayList;
import java.util.Iterator;

public class Proprietario {
    private final String cpf;
    private final String identidade;
    private String name;
    private Endereco endereco;
    private final ArrayList<Imovel> imoveis;

    public Proprietario(String name, String cpf, String identidade, String rua, String cep, String cidade, String estado, int numero) throws Exception {
        try {
            this.name = name;
            this.cpf = cpf;
            this.identidade = identidade;
            this.endereco = new Endereco(rua, numero, cidade, estado,  cep);
            this.imoveis = new ArrayList<>();
        } catch (Exception e) {
            throw e;
        }
    }

    public void atualizaEndereco(String rua, int numero, String cep) throws Exception {
        try {
            this.atualizaEndereco(rua, numero, cep, Estados.getStringFromEstados(this.endereco.getEstado()), this.endereco.getCidade());
        } catch (Exception e) {
            throw e;
        }
    }

    public void atualizaEndereco(String rua, int numero, String cep, String estado, String cidade) throws Exception {
        try {
            this.endereco = new Endereco(rua, numero, cidade, estado, cep);
        } catch (Exception e) {
            throw e;
        }
    }

    public String getName() {
        return name;
    }

    public String getCpf() {
        return cpf;
    }

    public String getIdentidade() {
        return identidade;
    }

    public ArrayList<Imovel> getImoveis() {
        return imoveis;
    }

    public ArrayList<Imovel> getImoveis(ImovelType tipo) {
        ArrayList<Imovel> filteredImoveis = new ArrayList<>();
        for (Imovel imovel : this.imoveis) {
            if (imovel.getTipo() == tipo) {
                filteredImoveis.add(imovel);
            }
        }

        return filteredImoveis;
    }

    public void addImovel(Imovel imovel) {
        this.imoveis.add(imovel);
    }

    public void removeImoveis(String IPTU) {
        for (Iterator<Imovel> it = this.imoveis.iterator(); it.hasNext();) {
            Imovel imovel = it.next();
            if(IPTU.equals(imovel.getIPTU())) {
                this.imoveis.remove(imovel);
                return;
            }
        }

        return;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setName(String name) {
        this.name = name;
    }
}
