public class ImovelAlreadyExists extends Exception {
  public ImovelAlreadyExists(String errMsg) {
    super(errMsg);
  }
}
