public class ProprietarioAlreadyExists extends Exception {
  public ProprietarioAlreadyExists(String errMsg) {
    super(errMsg);
  }
}
