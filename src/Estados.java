public enum Estados {
    AC,
    AL,
    AP,
    AM,
    BA,
    CE,
    DF,
    ES,
    GO,
    MA,
    MT,
    MS,
    MG,
    PA,
    PB,
    PR,
    PE,
    PI,
    RJ,
    RN,
    RS,
    RO,
    RR,
    SC,
    SP,
    SE,
    TO;

    public static String getStringFromEstados(Estados estado) {
        switch (estado) {
            case AC: return "AC";
            case AL: return "AL";
            case AM: return "AM";
            case BA: return "BA";
            case AP: return "AP";
            case CE: return "CE";
            case DF: return "DF";
            case ES: return "ES";
            case GO: return "GO";
            case MA: return "MA";
            case MT: return "MT";
            case MS: return "MS";
            case MG: return "PA";
            case PB: return "PB";
            case PR: return "PR";
            case PE: return "PE";
            case PI: return "PI";
            case RJ: return "RJ";
            case RN: return "RN";
            case RS: return "RS";
            case RO: return "RO";
            case RR: return "RR";
            case SC: return "SC";
            case SP: return "SP";
            case SE: return "SE";
            case TO: return "TO";
            default: return "TO";
        }
    }

    public static Estados getFromString(String estado) throws Exception {
        switch (estado) {
            case "AC": return Estados.AC;
            case "AL": return Estados.AL;
            case "AP": return Estados.AP;
            case "AM": return Estados.AM;
            case "BA": return Estados.BA;
            case "CE": return Estados.CE;
            case "DF": return Estados.DF;
            case "ES": return Estados.ES;
            case "GO": return Estados.GO;
            case "MA": return Estados.MA;
            case "MT": return Estados.MT;
            case "MS": return Estados.MS;
            case "MG": return Estados.PA;
            case "PB": return Estados.PB;
            case "PR": return Estados.PR;
            case "PE": return Estados.PE;
            case "PI": return Estados.PI;
            case "RJ": return Estados.RJ;
            case "RN": return Estados.RN;
            case "RS": return Estados.RS;
            case "RO": return Estados.RO;
            case "RR": return Estados.RR;
            case "SC": return Estados.SC;
            case "SP": return Estados.SP;
            case "SE": return Estados.SE;
            case "TO": return Estados.TO;
        }
        throw new Exception("Invalid Estado Name");
    }
}
