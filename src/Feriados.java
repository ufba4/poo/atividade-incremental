import java.time.LocalDate;

public class Feriados {
    private final LocalDate data;
    private final int mult;

    public Feriados(LocalDate data, int mult) {
        this.data = data;
        this.mult = mult;
    }

    public LocalDate getdata() {
        return this.data;
    }

    public int getMult() {
        return this.mult;
    }
}
