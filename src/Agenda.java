import java.time.LocalDate;
import java.util.ArrayList;

public class Agenda {
    private final ArrayList<LocalDate> dataAlugado;
    private final ArrayList<LocalDate> dataDisponivel;
    private final ArrayList<LocalDate> dataBloqueado;

    public Agenda() {
        this.dataDisponivel = new ArrayList<>();
        this.dataAlugado = new ArrayList<>();
        this.dataBloqueado = new ArrayList<>();
    }

    public ArrayList<LocalDate> getDisponvel() {
        return this.dataDisponivel;
    }

    public ArrayList<LocalDate> getAlugado() {
        return this.dataAlugado;
    }

    public ArrayList<LocalDate> getBloqueado() {
        return this.dataBloqueado;
    }

    public void setDisponivel(LocalDate data) {
        this.dataDisponivel.add(data);
    }

    public void setAlugado(LocalDate data) {
        this.dataAlugado.add(data);
    }

    public void setBloqueado(LocalDate data) {
        this.dataBloqueado.add(data);
    }

    private boolean compareDates(LocalDate data, ArrayList<LocalDate> agendaDatas) {
        for (LocalDate d : agendaDatas) {
            if (data.equals(d)) {
                return true;
            }
        }

        return false;
    }

    public boolean confereDisponivel(LocalDate data) {
        return this.compareDates(data, this.dataDisponivel);
    }

    public boolean confereAlugado(LocalDate data) {
        return this.compareDates(data, this.dataAlugado);
    }

    public boolean confereBloqueado(LocalDate data) {
        return this.compareDates(data, this.dataBloqueado);
    }
}