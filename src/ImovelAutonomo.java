public class ImovelAutonomo extends Imovel {
  private final float areaUtil;
  private final float areaConstruida;

  public ImovelAutonomo(String IPTU, String rua, int numero, String cep, String estado, String cidade, ImovelType tipo, ImovelUtilization utilizacao, float areaUtil, float areaConstruida) throws Exception {
    super(IPTU, rua, numero, cep, estado, cidade, tipo, utilizacao);
    this.areaUtil = areaUtil;
    this.areaConstruida = areaConstruida;
  }

  public ImovelAutonomo(String IPTU, String rua, int numero, String cep, ImovelType tipo, ImovelUtilization utilizacao, float areaUtil, float areaConstruida) throws Exception {
    this(IPTU, rua, numero, cep, "BA", "Salvador", tipo, utilizacao, areaUtil, areaConstruida);
  }

  public float getImovelRefValueSazon(int event) {
    return (this.areaConstruida * 15) + (this.areaConstruida * 15 * event);
  }

  public float getImovelRefValue() {
    return this.getImovelRefValueSazon(0);
  }

}
