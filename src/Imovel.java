import java.time.LocalDate;
import java.util.ArrayList;
import java.time.Month;
import java.util.stream.Collectors;

abstract public class Imovel implements AlugarImovel {
    private final String IPTU;
    private final ImovelType tipo;
    private final ImovelUtilization utilizacao;
    private final Endereco endereco;
    private final Agenda agenda;
    private final ArrayList<Feriados> feriados;

    public Imovel(String IPTU, String rua, int numero, String cep, String estado, String cidade, ImovelType tipo,
            ImovelUtilization utilizacao) throws Exception {
        try {
            this.IPTU = IPTU;
            this.tipo = tipo;
            this.utilizacao = utilizacao;
            this.endereco = new Endereco(rua, numero, cidade, estado, cep);
            this.agenda = new Agenda();
            this.feriados = new ArrayList<Feriados>();
            this.feriados.add(new Feriados(LocalDate.of(2024, Month.JANUARY, 1), 20));
            this.feriados.add(new Feriados(LocalDate.of(2023, Month.DECEMBER, 25), 10));
            this.feriados.add(new Feriados(LocalDate.of(2024, Month.FEBRUARY, 12), 15));
            this.feriados.add(new Feriados(LocalDate.of(2023, Month.NOVEMBER, 2), 5));
        } catch (Exception e) {
            throw e;
        }
    }

    public Imovel(String IPTU, String rua, int numero, String cep, ImovelType tipo, ImovelUtilization utilizacao)
            throws Exception {
        this(IPTU, rua, numero, cep, "BA", "Salvador", tipo, utilizacao);
    }

    abstract float getImovelRefValueSazon(int event);

    abstract float getImovelRefValue();

    public boolean imovelLivre(LocalDate dataInicial, LocalDate dataFinal) {
        ArrayList<LocalDate> dateArr = new ArrayList<>(dataInicial.datesUntil(dataFinal).collect(Collectors.toList()));

        for (LocalDate data : dateArr) {
            if (!this.getAgenda().confereDisponivel(data)) {
                return false;
            }
        }

        return true;
    }

    public float valorAluguel(LocalDate dataInicial, LocalDate dataFinal) {
        ArrayList<LocalDate> dateArr = new ArrayList<>(dataInicial.datesUntil(dataFinal).collect(Collectors.toList()));
        float sum = 0;

        for (LocalDate data : dateArr) {
            sum += valorAluguel(data);
        }

        return sum;
    }

    public float valorAluguel(LocalDate data) {
        ArrayList<Feriados> feriados = this.getFeriados();

        for (Feriados day : feriados) {
            if (day.getdata().equals(data)) {
                return this.getImovelRefValueSazon(day.getMult());
            }
        }

        return this.getImovelRefValue();
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public String getIPTU() {
        return IPTU;
    }

    public ImovelType getTipo() {
        return tipo;
    }

    public ImovelUtilization getUtilizacao() {
        return utilizacao;
    }

    public Agenda getAgenda() {
        return agenda;
    }

    public ArrayList<Feriados> getFeriados() {
        return feriados;
    }
}
