import java.util.Scanner;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Arrays;

public class Main {
  static ArrayList<Proprietario> proprietariosArr = new ArrayList<>();
  static ArrayList<Imovel> imovelArr = new ArrayList<>();

  public static void main(String[] args) {
    try {
      // Create mockup data
      Proprietario proprietario = new Proprietario("Maria", "1234567890", "0987654321", "Rua abc", "1234567",
          "Salvador", "BA", 10);
      proprietariosArr.add(proprietario);
      Imovel imovel = new ImovelAutonomo("123456789", "Rua 1", 10, "1234567", "BA", "Salvador", ImovelType.CASA, ImovelUtilization.CAMPO, 100f, 50f);
      imovelArr.add(imovel);
      ArrayList<String> lazerSegundoImovel = new ArrayList<>();
      lazerSegundoImovel.add("Piscina");
      Imovel segundoImovel = new ImovelCompartilhado("123456789", "Rua 1", 10, "1234567", ImovelType.CASA, ImovelUtilization.CAMPO, lazerSegundoImovel, "10");
      imovelArr.add(segundoImovel);
      Imovel terceiroImovel = new ImovelAutonomo("123456780", "Rua 2", 11, "1234560", ImovelType.APTO, ImovelUtilization.PRAIA, 1000f, 1000f);
      imovelArr.add(terceiroImovel);
      proprietario.addImovel(imovel);
      proprietario.addImovel(segundoImovel);
      proprietario.addImovel(terceiroImovel);
      proprietario.removeImoveis(imovel.getIPTU());
      proprietario.atualizaEndereco("Rua def", 101, "345678", "BA", "Feira de Santana");
      Scanner scan = new Scanner(System.in);
      System.out.println("O que deseja fazer?");
      System.out.println("1 - Cadastrar novo Imovel");
      System.out.println("2 - Cadastrar novo proprietario");
      int userChoice = Integer.parseInt(scan.nextLine());
      System.out.println(imovel.imovelLivre(LocalDate.of(2024, Month.APRIL, 1), LocalDate.of(2024, Month.APRIL, 10)));
      System.out.println(imovel.valorAluguel(LocalDate.of(2023, Month.DECEMBER, 20), LocalDate.of(2024, Month.JANUARY, 2)));

      switch (userChoice) {
        case 1:
          cadastraImovel(scan);
          System.out.println("Imovel Cadastrado!");
          break;
        case 2:
          cadastraProprietario(scan);
          System.out.println("Proprietario cadastrado!");
          break;
        default:
          System.out.println("Ação Invalida");
          break;
      }
    } catch (Exception e) {
      System.out.println(e);
    }

  }

  public static Proprietario checkProprietarioExiste(String cpf) {
    for (Proprietario p: proprietariosArr) {
      if (p.getCpf().equals(cpf)) {
        return p;
      }
    }

    return null;
  }

  public static Imovel checkImovelExiste(String iptu) {
    for (Imovel i: imovelArr) {
      if (i.getIPTU().equals(iptu)) {
        return i;
      }
    }

    return null;
  }

  public static Imovel cadastraImovel(Scanner scan) throws Exception, ImovelAlreadyExists {
    System.out.println("Cadastrando novo Imovel...");
    System.out.println("Qual o IPTU?");
    String iptu = scan.nextLine();
    System.out.println("Qual a rua?");
    String rua = scan.nextLine();
    System.out.println("Qual o numero do Imovel?");
    int numero = Integer.parseInt(scan.nextLine());
    System.out.println("Qual o Estado?");
    String estado = scan.nextLine();
    System.out.println("Qual a cidade?");
    String cidade = scan.nextLine();
    System.out.println("Qual o CEP?");
    String cep = scan.nextLine();

    System.out.println("Qual o tipo de imovel?");
    System.out.println("1 - Casa");
    System.out.println("2 - Apto");
    ImovelType type;
    int imovelTypeInt = Integer.parseInt(scan.nextLine());

    switch (imovelTypeInt) {
      case 2:
        type = ImovelType.APTO;
        break;
      default:
        type = ImovelType.CASA;
        break;
    }

    System.out.println("Qual a utilização do imovel?");
    System.out.println("1 - Campo");
    System.out.println("2 - Praia");
    ImovelUtilization utilization;
    int imovelUtilizationInt = Integer.parseInt(scan.nextLine());

    switch (imovelUtilizationInt) {
      case 2:
        utilization = ImovelUtilization.PRAIA;
        break;
      default:
        utilization = ImovelUtilization.CAMPO;
        break;
    }

    System.out.println("Qual o estilo de Imovel?");
    System.out.println("1 - Autonomo");
    System.out.println("2 - Compartilhado");
    int imovelStyleInt = Integer.parseInt(scan.nextLine());

    if (checkImovelExiste(iptu) != null) {
      throw new ImovelAlreadyExists("Imovel com IPTU " + iptu + " already exists");
    }

    switch (imovelStyleInt) {
      case 1:
        System.out.println("Qual a área util?");
        float areaUtil = Float.parseFloat(scan.nextLine());
        System.out.println("Qual a área construida?");
        float areaConstruida = Float.parseFloat(scan.nextLine());
        return (Imovel) new ImovelAutonomo(iptu, rua, numero, cep, estado, cidade, type, utilization, areaUtil, areaConstruida);
      default:
        System.out.println("Qual a identificação?");
        String identificacao = scan.nextLine();
        System.out.println("Qual o lazer (separados por ,)");
        ArrayList<String> lazer = new ArrayList<>(Arrays.asList(scan.nextLine().split(",")));
        return (Imovel) new ImovelCompartilhado(iptu, rua, numero, cep, estado, cidade, type, utilization, lazer, identificacao);
    }
  }

  public static Proprietario cadastraProprietario(Scanner scan) throws Exception, ProprietarioAlreadyExists {
    System.out.println("Cadastrando novo proprietario");
    System.out.println("Qual o nome?");
    String nome = scan.nextLine();
    System.out.println("Qual o seu CPF?");
    String cpf = scan.nextLine();
    System.out.println("Qual a sua identidade?");
    String identidade = scan.nextLine();
    System.out.println("Qual a sua rua?");
    String rua = scan.nextLine();
    System.out.println("Qual a sua cidade?");
    String cidade = scan.nextLine();
    System.out.println("Qual o seu estado?");
    String estado = scan.nextLine();
    System.out.println("Qual o seu numero?");
    int numero = Integer.parseInt(scan.nextLine());
    System.out.println("Qual o seu CEP?");
    String cep = scan.nextLine();

    if (checkProprietarioExiste(cpf) != null) {
      throw new ProprietarioAlreadyExists("Proprietario com cpf " + cpf + " ja existe");
    }

    return new Proprietario(nome, cpf, identidade, rua, cep, cidade, estado, numero);
  }
}
