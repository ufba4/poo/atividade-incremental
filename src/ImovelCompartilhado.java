import java.util.ArrayList;

public class ImovelCompartilhado extends Imovel {
    private final ArrayList<String> lazer;
    private final String identificacao;

    public ImovelCompartilhado(String IPTU, String rua, int numero, String cep, String estado, String cidade, ImovelType tipo, ImovelUtilization utilizacao, ArrayList<String> lazer, String identificacao) throws Exception {
      super(IPTU, rua, numero, cep, estado, cidade, tipo, utilizacao);
      this.lazer = lazer;
      this.identificacao = identificacao;
    }


    public ImovelCompartilhado(String IPTU, String rua, int numero, String cep, ImovelType tipo, ImovelUtilization utilizacao, ArrayList<String> lazer, String identificacao) throws Exception {
      this(IPTU, rua, numero, cep, "BA", "Salvador", tipo, utilizacao, lazer, identificacao);
    }

    public float getImovelRefValueSazon(int event) {
      float iptu = Float.parseFloat(super.getIPTU());
      float resp = (iptu * this.lazer.size()) + (iptu * this.lazer.size() * event);

      if (this.lazer.size() == 0 ) {
        resp *= 0.9;
      }

      return resp;
    }

    public float getImovelRefValue() {
      return this.getImovelRefValueSazon(0);
    }
}
