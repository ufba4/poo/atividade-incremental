import java.time.LocalDate;

interface AlugarImovel {
  boolean imovelLivre(LocalDate dataInicial, LocalDate dataFinal);
  float valorAluguel(LocalDate dataInicial, LocalDate dataFinal);
  float valorAluguel(LocalDate data);
}
