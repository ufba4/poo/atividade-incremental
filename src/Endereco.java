public class Endereco {
    private final String rua;
    private final int numero;
    private final String cidade;
    private final String cep;
    private final Estados estado;

    public Endereco(String rua, int numero, String cidade, String estado, String cep) throws Exception {
        try {
            this.estado = this.validateEstado(estado);
            this.rua = rua;
            this.numero = numero;
            this.cidade = cidade;
            this.cep = cep;
        } catch (Exception e) {
            throw e;
        }
    }

    private Estados validateEstado(String estado) throws Exception {
        try {
            return Estados.getFromString(estado);
        } catch (Exception e) {
            throw e;
        }
    }

    public String getRua() {
        return rua;
    }

    public int getNumero() {
        return numero;
    }

    public String getCidade() {
        return cidade;
    }

    public Estados getEstado() {
        return estado;
    }

    public String getCep() {
        return cep;
    }
}
